#include <stdio.h>
#include "dice.h"
//isso é um comentario
int main(){
	int n;
	initializeSeed();
	printf("\tHow many sides does your dice have? ");
	scanf("%d", &n);
	printf("Let's roll the dice: %d\n", rollDice(n));
	return 0;
}
